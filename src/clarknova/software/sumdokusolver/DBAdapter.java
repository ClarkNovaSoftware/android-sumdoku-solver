package clarknova.software.sumdokusolver;

import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {
	private final static String TAG = "sumdoku";

	public static final String KEY_ROWID = "_id";

	public static final String KEY_SAVENAME = "save_name";
	public static final String KEY_VARS = "vars";
	public static final String KEY_CELL_NUM = "cell_num";
	public static final String KEY_SUM_NUM = "sum_num";
	public static final String KEY_SUM = "sum";

	private static final String DATABASE_NAME = "sumdokusolver";
	private static final String DB_CELLS_TABLE = "cells_table";
	private static final String DB_SUMS_TABLE = "sums_table";
	private static final int DATABASE_VERSION = 2;

	private static final String DB_CELLS_CREATE = "create table IF NOT EXISTS "
			+ DB_CELLS_TABLE + " (" + KEY_ROWID
			+ " integer primary key autoincrement, " + KEY_SAVENAME
			+ " text not null, " + KEY_CELL_NUM + " integer, " + KEY_VARS
			+ " integer);";

	private static final String DB_SUMS_CREATE = "create table IF NOT EXISTS "
			+ DB_SUMS_TABLE + " (" + KEY_ROWID
			+ " integer primary key autoincrement, " + KEY_SAVENAME
			+ " text not null, " + KEY_SUM_NUM + " integer, " + KEY_SUM
			+ " integer, " + KEY_CELL_NUM + " integer);";
	static String t = "ttt";
	private static final String DB_CELLS_ADDCOLUMN = "alter table "
			+ DB_CELLS_TABLE + " add column " + KEY_SAVENAME
			+ " text not null default \'" + t + "\';";
	private static final String DB_SUMS_ADDCOLUMN = "alter table "
			+ DB_SUMS_TABLE + " add column " + KEY_SAVENAME
			+ " text not null default \'" + t + "\';";

	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public DBAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DB_CELLS_CREATE);
			db.execSQL(DB_SUMS_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion);

			Log.v(TAG, "sql: " + DB_CELLS_ADDCOLUMN);
			db.execSQL(DB_CELLS_ADDCOLUMN);
			Log.v(TAG, "sql: " + DB_SUMS_ADDCOLUMN);
			db.execSQL(DB_SUMS_ADDCOLUMN);
		}

	}

	// ---opens the database---
	public DBAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	public Vector<String> allsavenames() {
		open();
		int countOfRecords;
		String CountOfRecords = "SELECT COUNT(" + KEY_SAVENAME
				+ ") FROM (SELECT DISTINCT " + KEY_SAVENAME + " FROM "
				+ DB_CELLS_TABLE + ");";
		Cursor cursor = db.rawQuery(CountOfRecords, null);
		cursor.moveToFirst();
		countOfRecords = cursor.getInt(0);
		Log.v(TAG, "Count of records == " + countOfRecords);
		if (countOfRecords == 0) {
			close();
			return null;
		}
		String getRecords = "SELECT DISTINCT " + KEY_SAVENAME + " FROM "
				+ DB_CELLS_TABLE + ";";
		cursor = db.rawQuery(getRecords, null);
		cursor.moveToFirst();
		Vector<String> savenamesvector = new Vector<String>();
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			String name = cursor.getString(0);
			savenamesvector.add(name);
		}
		close();
		return savenamesvector;

	}

	public Cell[] loadfield(String save_name) {
		open();
		int countOfRecords;
		String CountOfRecords = "SELECT COUNT(*) FROM " + DB_CELLS_TABLE
				+ " WHERE (" + KEY_SAVENAME + " = \'" + save_name + "\');";
		Cursor cursor = db.rawQuery(CountOfRecords, null);
		cursor.moveToFirst();
		countOfRecords = cursor.getInt(0);
		Log.v(TAG, "Count of records == " + countOfRecords);
		if (countOfRecords == 0) {
			close();
			return null;
		}
		String getRecords = "SELECT * FROM " + DB_CELLS_TABLE + " WHERE ("
				+ KEY_SAVENAME + " = \'" + save_name + "\');";
		cursor = db.rawQuery(getRecords, null);
		cursor.moveToFirst();
		Cell cells[] = new Cell[countOfRecords];
		int i = 0;
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			int cell_num = cursor.getInt(cursor.getColumnIndex(KEY_CELL_NUM));
			int vars = cursor.getInt(cursor.getColumnIndex(KEY_VARS));
			cells[i] = (new Cell(cell_num, vars, context));
			i++;
		}
		close();
		return cells;
	}
	
	public void savedata(Data data, String save_name) {
		Log.d(TAG, "savedata starting");
		open();
		db.beginTransaction();
		try {
			
			for (Cell cell : data.field) {
				ContentValues initialValues = new ContentValues();
				initialValues.put(KEY_SAVENAME, save_name);
				initialValues.put(KEY_CELL_NUM, cell.num);
				initialValues.put(KEY_VARS, cell.var_value);
				db.insert(DB_CELLS_TABLE, null, initialValues);
			}
			
			int sumnum = 0;
			for (Sum sum : data.sums) {
				for (int cell_num : sum.cell_nums) {
					ContentValues initialValues = new ContentValues();
					// Log.d(TAG, "  "+sumnum+" | "+sum.sum+" | "+cell_num);
					initialValues.put(KEY_SAVENAME, save_name);
					initialValues.put(KEY_SUM_NUM, sumnum);
					initialValues.put(KEY_SUM, sum.sum);
					initialValues.put(KEY_CELL_NUM, cell_num);
					db.insert(DB_SUMS_TABLE, null, initialValues);
					// Log.v(TAG,initialValues.valueSet()+"");
				}
				
				sumnum++;	
			}
			
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
		
		close();
		Log.d(TAG, "savedata finished");
	}
	
//	public void savefield(Cell[] field, String save_name) {
//		Log.d(TAG, "savefield starting");
//		open();
//		db.beginTransaction();
//		try {
//			for (Cell cell : field) {
//				ContentValues initialValues = new ContentValues();
//				initialValues.put(KEY_SAVENAME, save_name);
//				initialValues.put(KEY_CELL_NUM, cell.num);
//				initialValues.put(KEY_VARS, cell.var_value);
//				db.insert(DB_CELLS_TABLE, null, initialValues);
//			}
//			db.setTransactionSuccessful();
//		} finally {
//			db.endTransaction();
//		}
//
//		close();
//		Log.d(TAG, "savefield finished");
//	}

	public boolean saveexists(String save_name) {
		open();
		int countOfRecords;
		String CountOfRecords = "SELECT COUNT(*) FROM " + DB_CELLS_TABLE
				+ " WHERE (" + KEY_SAVENAME + " = \'" + save_name + "\');";
		Cursor cursor = db.rawQuery(CountOfRecords, null);
		cursor.moveToFirst();
		countOfRecords = cursor.getInt(0);
		if (countOfRecords > 0) {
			return true;
		}
		CountOfRecords = "SELECT COUNT(*) FROM " + DB_SUMS_TABLE + " WHERE ("
				+ KEY_SAVENAME + " = \'" + save_name + "\');";
		cursor = db.rawQuery(CountOfRecords, null);
		cursor.moveToFirst();
		countOfRecords = cursor.getInt(0);
		if (countOfRecords > 0) {
			return true;
		}
		return false;
	}

	public Vector<Sum> loadSums(String save_name) {
		Vector<Sum> sums = new Vector<Sum>();
		open();
		int countOfRecords;
		String CountOfRecords = "SELECT COUNT(*) FROM " + DB_SUMS_TABLE
				+ " WHERE (" + KEY_SAVENAME + " = \'" + save_name + "\');";
		Cursor cursor = db.rawQuery(CountOfRecords, null);
		cursor.moveToFirst();
		countOfRecords = cursor.getInt(0);
		// Log.v(TAG, "Count of records == " + countOfRecords);
		if (countOfRecords == 0) {
			close();
			return null;
		}
		String getMaxsumnum = "select MAX(" + KEY_SUM_NUM + ") FROM "
				+ DB_SUMS_TABLE + " WHERE (" + KEY_SAVENAME + " = \'"
				+ save_name + "\');";
		cursor = db.rawQuery(getMaxsumnum, null);
		cursor.moveToFirst();
		int maxsumnum = cursor.getInt(0);
		// Log.d(TAG, maxsumnum+"");
		for (int i = 0; i <= maxsumnum; i++) {
			String getRecords = "SELECT * FROM " + DB_SUMS_TABLE + " WHERE ("
					+ KEY_SUM_NUM + " = \'" + i + "\' and " + KEY_SAVENAME
					+ " = \'" + save_name + "\');";
			cursor = db.rawQuery(getRecords, null);
			cursor.moveToFirst();
			int sum = 0;
			try {
				sum = cursor.getInt(cursor.getColumnIndex(KEY_SUM));
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			Vector<Integer> cellnums = new Vector<Integer>();
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor
					.moveToNext()) {
				/*
				 * Log.d(TAG, cursor.getInt(cursor.getColumnIndex(KEY_SUM_NUM))+
				 * " "+cursor.getInt(cursor.getColumnIndex(KEY_SUM))+
				 * " "+cursor.getInt(cursor.getColumnIndex(KEY_CELL_NUM)));
				 */
				cellnums.add(cursor.getInt(cursor.getColumnIndex(KEY_CELL_NUM)));
			}
			sums.add(new Sum(sum, cellnums, context));
			// Log.d(TAG, "-= "+sum+" =--= "+cellnums+" =-");
		}

		close();
		return sums;
	}

//	public void saveSums(Vector<Sum> sums, String save_name) {
//		Log.d(TAG, "Saving sums, opening db...");
//		open();
//		Log.d(TAG, "opened");
//
//		int sumnum = 0;
//		for (Sum sum : sums) {
//			// Log.d(TAG, "Saving sum "+sum.sum+" ("+sum.cell_nums+")");
//			db.beginTransaction();
//			try {
//				for (int cell_num : sum.cell_nums) {
//					ContentValues initialValues = new ContentValues();
//					// Log.d(TAG, "  "+sumnum+" | "+sum.sum+" | "+cell_num);
//					initialValues.put(KEY_SAVENAME, save_name);
//					initialValues.put(KEY_SUM_NUM, sumnum);
//					initialValues.put(KEY_SUM, sum.sum);
//					initialValues.put(KEY_CELL_NUM, cell_num);
//					db.insert(DB_SUMS_TABLE, null, initialValues);
//					// Log.v(TAG,initialValues.valueSet()+"");
//				}
//
//				db.setTransactionSuccessful();
//			} finally {
//				db.endTransaction();
//			}
//
//			sumnum++;
//		}
//		Log.d(TAG, "closing...");
//		close();
//		Log.d(TAG, "closed, Saving sums finished");
//	}

	public void clearSaving(String save_name) {
		open();
		String delete = "Delete FROM " + DB_CELLS_TABLE + " WHERE ("
				+ KEY_SAVENAME + " = \'" + save_name + "\');";
		Log.v(TAG, delete);
		Cursor cursor = db.rawQuery(delete, null);
		if (cursor.moveToFirst()) {
			Log.v(TAG, "" + cursor.getInt(0));
		}

		delete = "Delete FROM " + DB_SUMS_TABLE + " WHERE (" + KEY_SAVENAME
				+ " = \'" + save_name + "\');";
		Log.v(TAG, delete);
		cursor = db.rawQuery(delete, null);
		if (cursor.moveToFirst()) {
			Log.v(TAG, "" + cursor.getInt(0));
		}

		close();
	}

}
