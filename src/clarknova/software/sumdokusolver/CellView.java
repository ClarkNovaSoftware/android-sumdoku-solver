package clarknova.software.sumdokusolver;

import java.util.Vector;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class CellView extends RelativeLayout {
	private AndroidSumdokuSolver sumdoku;
	private final String TAG = "sumdoku";
	public TextView t = (TextView) this.findViewById(R.id.value);
	private Calculate calculate;
	Toast toast;

	public CellView(Context context) {
		super(context);
		sumdoku = (AndroidSumdokuSolver) context;
		calculate = sumdoku.calculate;
		toast = sumdoku.toast;

		setFocusable(true);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (inflater != null) {
			inflater.inflate(R.layout.cell, this);
		}
		hideborders();
		setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				itemClick(v);
			}
		});
		setOnLongClickListener(new View.OnLongClickListener() {

			// @Override
			public boolean onLongClick(final View v) {
				boolean res = false;
				Context context = getContext();
				getvalue(context, v);
				return res;
			}
		});

	}

	protected void getvalue(Context context, final View v) {
		final Dialog dialog = new Dialog(context);

		dialog.setContentView(R.layout.dialog_pick_a_value);
		dialog.setTitle(R.string.title_set_value);
		Cell cell = ((CellView) v).getCell();

		Vector<Integer> vars = calculate.nums_vector(sumdoku.data.find_possible_vars(cell));

		Log.v(TAG, "Get value for cell #" + cell.num + ", vars = " + vars);
		TableLayout layout = (TableLayout) dialog.findViewById(R.id.table_dialog);
		int n = 1;
		for (int i = 0; i < Calculate.SUMDOKU_SIZE / 3; i++) {
			TableRow row = new TableRow(context);
			for (int k = 0; k < Calculate.SUMDOKU_SIZE / 3; k++) {
				//Log.v(TAG, "Button number " + n);
				Button num = new Button(context);

				num.setEnabled(vars.contains(n));
				num.setText("" + n);
				num.setOnClickListener(new View.OnClickListener() {
					public void onClick(View value_button) {
						int val = Integer.parseInt(((Button) value_button)
								.getText().toString());
						((CellView) v).setvalue(val);
						dialog.dismiss();
					}
				});

				row.addView(num);
				TableRow.LayoutParams params = (TableRow.LayoutParams) num
						.getLayoutParams();
				params.width = LayoutParams.MATCH_PARENT;
				params.height = LayoutParams.WRAP_CONTENT;
				params.weight = 1;
				num.setLayoutParams(params);
			
				n++;
			}
			layout.addView(row, new TableLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		}
		Button zero = (Button) dialog.findViewById(R.id.zero);
		zero.setOnClickListener(new View.OnClickListener() {
			public void onClick(View w) {
				((CellView) v).clearvalue();
				dialog.dismiss();
			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancel_dialog);
		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View w) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	private Cell getCell() {
		int num = Integer.parseInt(getTag().toString());
		Cell cell = sumdoku.data.getcell(num);
		Log.v(TAG, "Cell: num " + cell.num + ", var_value " + cell.var_value);
		return cell;
	}

	public void setvalue(int value) {
		// CellView cellView = (CellView) cv;
		TextView t = (TextView) findViewById(R.id.value);
		t.setTextAppearance(sumdoku, android.R.style.TextAppearance_Large);
		t.setTextColor(Color.BLACK);
		t.setTypeface(null, 1);

		t.setText("" + value);

		TextView v = (TextView) findViewById(R.id.vars);
		v.setText("");

		// Log.v(TAG, "set: num = "+num+", val = "+value);
		Cell cell = getCell();
		sumdoku.data.setValue(value, cell.num);
		
	}

	public void clearvalue() {
		TextView t = (TextView) this.findViewById(R.id.value);
		t.setTypeface(null, 0);
		t.setText("");
		int num = Integer.parseInt(getTag().toString());
		
		// Log.v(TAG, "set: num = "+num+", val = "+val);
		Cell cell = new Cell(num, sumdoku);
		cell.saveValue(0);
		sumdoku.data.setfield(cell);
	}

	private void itemClick(View v) {
		View b = findViewById(R.id.back);
		int num = Integer.parseInt(this.getTag().toString());
		// Log.v(TAG,
		// "num="+num+", selected_cells="+AndroidSumdokuSolver.selected_cells);
		Sum sum = sumdoku.data.getsum(num);
		if (sum != null) {
			if (sumdoku.selected_cells.isEmpty()) {
				sumdoku.editSum(num);
				return;
			}
			toast.setText(num + " is already in sum ");
			Log.v(TAG, num + " is already in sum ");
			toast.show();
			return;
		}
		if (!sumdoku.selected_cells.contains(num)
				&& sumdoku.selected_cells.size() < Calculate.SUMDOKU_SIZE - 1) {
			b.setBackgroundColor(Color.GRAY);
			sumdoku.selected_cells.add(num);
		} else {
			b.setBackgroundColor(Color.WHITE);
			sumdoku.selected_cells.removeElement(num);
		}
		sumdoku.get_sum_button.setEnabled(sumdoku.selected_cells.size()>1);
		sumdoku.remove_button.setEnabled(sumdoku.selected_cells.size()>0);
		// Log.v(TAG,""+AndroidSumdokuSolver.selected_cells);
	}

	public void hideborders() {
		findViewById(R.id.tempback).setVisibility(INVISIBLE);
		findViewById(R.id.border_bottom).setVisibility(INVISIBLE);
		findViewById(R.id.border_bottom_lr).setVisibility(INVISIBLE);
		findViewById(R.id.border_bottom_rl).setVisibility(INVISIBLE);
		findViewById(R.id.border_left).setVisibility(INVISIBLE);
		findViewById(R.id.border_left_bt).setVisibility(INVISIBLE);
		findViewById(R.id.border_left_tb).setVisibility(INVISIBLE);
		findViewById(R.id.border_top).setVisibility(INVISIBLE);
		findViewById(R.id.border_top_lr).setVisibility(INVISIBLE);
		findViewById(R.id.border_top_rl).setVisibility(INVISIBLE);
		findViewById(R.id.border_right).setVisibility(INVISIBLE);
		findViewById(R.id.border_right_bt).setVisibility(INVISIBLE);
		findViewById(R.id.border_right_tb).setVisibility(INVISIBLE);
	}

	public void clear() {
		View b = findViewById(R.id.back);
		TextView sum_textview = (TextView) findViewById(R.id.sum);
		sum_textview.setText("");
		b.setBackgroundColor(Color.WHITE);
		b.invalidate();
		this.invalidate();
		hideborders();
	}

}
