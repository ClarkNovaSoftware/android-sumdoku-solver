package clarknova.software.sumdokusolver;
import java.util.Vector;

import android.content.Context;


public class Sum {
	public int sum;
	public Vector<Integer> cell_nums;
	public Sum(int sum, Context context) {
		this.sum=sum;
		this.cell_nums = new Vector<Integer>();
	}
	
	public Sum(int sum, Vector<Integer> cell_nums, Context context) {
		this.sum=sum;
		this.cell_nums = new Vector<Integer>();
		this.cell_nums.addAll(cell_nums);
	}

}
