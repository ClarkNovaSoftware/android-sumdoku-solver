package clarknova.software.sumdokusolver;

import android.content.Context;

//import android.util.Log;

public class Cell {
//	private static final String TAG = "sumsol";
	int num;
	int var_value;
	private int value;
	private AndroidSumdokuSolver sumdoku;
	
	public Cell(int num, Context context) {
		sumdoku = (AndroidSumdokuSolver) context;
		this.num = num;
	    this.var_value = (int) (Math.pow(2, Calculate.SUMDOKU_SIZE)-1);
	    this.value=0;
	}
	public Cell(int num, int vars, Context context) {
		sumdoku = (AndroidSumdokuSolver) context;
		Calculate calculate = sumdoku.calculate;
		this.num = num;
	    this.var_value = vars;
//	    if (calculate.nums_vector(vars).size()==1){
	    if (calculate.bit_vector(vars).size()==1){
	    	this.value=calculate.number(vars);
	    }
	    else this.value=0;
	    
	}
	public boolean equals(Cell c){
		return (this.num==c.num && this.value==c.value);
		 
	 }

	 
	 public void saveValue(int v){
			value=v;
			if (v!=0) {
				var_value=(int) (Math.pow(2, v-1));
				//Log.v(TAG, "cell "+this.num+" = "+v);
			}
			else { 
				Calculate calculate = sumdoku.calculate;
			    var_value = calculate.max_var();
			    value=0;
			}
		}

	 

	public int getValue() {
		return value;
	}
	
	public boolean remove_var(int var){
		boolean removed = false;
		int old_var_value = this.var_value;
		var_value = sumdoku.calculate.remove_var(var, old_var_value);
		if (old_var_value != this.var_value) {
			removed = true;
		}
	
		return removed;
	}
}
