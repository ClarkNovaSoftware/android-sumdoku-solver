package clarknova.software.sumdokusolver;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HelpScreen extends Activity {
	

	private String TAG = "sumdoku";

	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		Log.v(TAG, "Showing help");
		setContentView(R.layout.helpscren);
		LinearLayout help_LinearLayout = (LinearLayout) findViewById(R.id.help_linear_layout);
		for (Pair<String, Integer> pair : getPairs()) {
			View help_item = new View(this);
			LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			help_item = mInflater.inflate(R.layout.help_item, null);
			TextView help_txt = (TextView) help_item.findViewById(R.id.help_txt);
			help_txt.setText(pair.first);
			ImageView help_img = (ImageView) help_item.findViewById(R.id.help_img);
			help_img.setImageResource(pair.second);
			help_LinearLayout.addView(help_item);
		}
	}

	private List<Pair<String, Integer>> getPairs(){
		String [] help_txt = getResources().getStringArray(R.array.help_txt);
		int [] help_img = new int[]{
				R.drawable.screenshot_01,
				R.drawable.screenshot_02,
				R.drawable.screenshot_03,
				R.drawable.screenshot_04,
				R.drawable.screenshot_05,
				R.drawable.screenshot_06,
				R.drawable.screenshot_07};
		Vector<Pair<String, Integer>> t = new Vector<Pair<String,Integer>>();
		for (int i=0;i<help_img.length;i++){
			t.add(new Pair<String, Integer>(help_txt[i], help_img[i]));
			
		}
		for (Pair<String, Integer> pair : t) {
			Log.v(TAG, "Pair: "+pair.first+" "+pair.second.toString());
			
		}
		return t;
	}
	

}