package clarknova.software.sumdokusolver;

import java.util.Vector;

public class Calculate {
	public final static int SUMDOKU_SIZE=9;
	
	public Calculate() {
		fill_combinations();
	}
	public int totalsum() {
		return maxsum()*SUMDOKU_SIZE;
	}
	public int maxsum(int size) {
		int maxsum = 0;
		for (int i = 0; i < size; i++) {
			maxsum += (SUMDOKU_SIZE - i);
		}
		return maxsum;
	}

	public  int maxsum() {
		return maxsum(SUMDOKU_SIZE);
	}
	
	public  int minsum(int size) {
		int minsum = 0;
		for (int i = 1; i <= size; i++) {
			minsum += i;
		}
		return minsum;
	}
	public   Vector<Vector<Vector<Integer>>> combinations = new Vector<Vector<Vector<Integer>>>();

	public  void fill_combinations() {
		for (int size = 2; size <= SUMDOKU_SIZE; size++) {
			Vector<Vector<Integer>> sz = new Vector<Vector<Integer>>();
			for (int sum = minsum(size); sum <= maxsum(size); sum++) {
				sz.add(binaryset(size, sum));
			}
			combinations.add(sz);
		}
	}

	public   Vector<Integer> get_combinations(int size, int sum) {
		Vector<Integer> c = new Vector<Integer>();
		int sum_position = sum - minsum(size);
		int size_position = size - 2;
		if (sum_position >= 0 && size_position >= 0) {
			Vector<Vector<Integer>> cc = combinations.elementAt(size_position);
			if (cc.size()>sum_position){
				c = cc.elementAt(sum_position);
			}
			else {System.out.println(sum_position+">="+cc.size());}
		}
		return c;
	}
	
	public int num_to_location(int cellnum) {
		return ((SUMDOKU_SIZE) * (cellnum / 10) + (cellnum % 10));
	}

	public int bits_to_int(Vector<Integer> bits) {
		int res = 0;
		for (Integer b : bits) {
			res |= 1 << (b - 1);
		}
		return res;
	}

	public  int binarysettoint(Vector<Integer> b) {
		// System.out.print(b);
		int var = 0;
		while (!b.isEmpty()) {
			var = var | b.remove(0);
		}
		// System.out.println(" => "+var);
		return var;
	}

	public int bits(int s) {
		int bits = 0;
		for (int i = 0; i < SUMDOKU_SIZE; i++) {
			if ((s & (1 << i)) != 0) {
				bits++;
			}
		}
		// System.out.println(s+" .. "+bits+" bits");
		return bits;
	}

	public int all_vars(Sum sum){
		int set=0;
		for (int s : get_combinations(sum.cell_nums.size(), sum.sum)) {
			set|=s;
		}
		return set;
	}
	
	/**
	 * Bit intersection of two values
	 * @param set1 - first value representing set of bits
	 * @param set2 - second value representing set of bits
	 * @return int value representing intersection set of bits
	 */
	public int intersection (int set1, int set2){
		return set1&set2;
	}
	
	public   Vector<Integer> binaryset(int size, int sum) {
		Vector<Integer> sets = new Vector<Integer>();
		for (int n = 0; n < Math.pow(2, SUMDOKU_SIZE); n++) {
			int sz = 0;
			int sm = 0;
			for (int i = 0; i < SUMDOKU_SIZE; i++) {
				if (((1 << i) & n) != 0) {
					sz += 1;
					sm += i + 1;
				}
			}
			if ((sz == size) && (sm == sum)) {
				sets.add(n);
			}
		}
		return sets;
	}

	public int number(int var_value) {
		return bit_vector(var_value).firstElement() + 1;
//		return nums_vector(var_value).firstElement() + 1;
	}

	/**
	 * @param var_value
	 * @return vector of numbers of bits in inputed int, which are set as 1 
	 */
	public Vector<Integer> bit_vector(int var_value) {
		Vector<Integer> result = new Vector<Integer>();
		for (int i = 0; i < SUMDOKU_SIZE; i++) {
			if ((var_value & (1 << i)) != 0) {
				result.add(i);
			}
		}
		return result;
	}

	/**
	 * @param var_value
	 * @return set of possible variants of value in cell with this var_value
	 */
	public Vector<Integer> nums_vector(int var_value) {
		Vector<Integer> result = new Vector<Integer>();
		for (int i = 0; i < SUMDOKU_SIZE; i++) {
			if ((var_value & (1 << i)) != 0) {
				result.add(i + 1);
			}
		}
		return result;
	}



	public  Vector<Vector<Integer>> digitset(int size, int sum) {
		Vector<Vector<Integer>> sets = new Vector<Vector<Integer>>();
		for (int num : binaryset(size, sum)) {
			Vector<Integer> set = new Vector<Integer>();
			for (int i = 0; i < SUMDOKU_SIZE; i++) {
				if (((1 << i) & num) != 0) {
					set.add(i + 1);
				}
			}
			sets.add(set);
		}
		return sets;
	}
	
	protected String vars_to_string(int var_value) {
		String v="";
		for (int m = 0; m < SUMDOKU_SIZE; m++) {
			if (((1 << m) & var_value) != 0) {v += (m + 1);} 
			else {v += "-";}
		}
//		if (var_value==0){Log.e(TAG, "EMPTY CELL!!!");}
		return v;
	}
	
	public int min(Vector<Integer> nums) {
		int t=10*(SUMDOKU_SIZE+1)+SUMDOKU_SIZE+1;
		for (int n:nums){
			if (n<t){t=n;}
		}
		return t;
	}

	public int remove_var(int var, int var_value) {
		int t = var_value;
		t &= ~(1 << (var-1));
		return t;
	}

	public int max_var() {
		return (int) (Math.pow(2, SUMDOKU_SIZE)-1);
	}

	
}
