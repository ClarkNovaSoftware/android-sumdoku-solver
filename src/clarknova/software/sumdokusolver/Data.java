package clarknova.software.sumdokusolver;

import java.util.Vector;

import android.content.Context;
import android.util.Log;
//import android.app.Application;

public class Data {

	private static final int NOTCOMPLETE = 0;
	private static final int SOLVED = 1;
	private static final int EMPTYCELL = -1;

	private  final String TAG = "sumdoku";
	
	private  String wronginput;
	
	private AndroidSumdokuSolver sumdoku;
	public Vector<Sum> sums = new Vector<Sum>();
	private Calculate calculate;
	private Solve solve;
	private final int N;
	public Cell[] field;

	Data(Context context){
		sumdoku= (AndroidSumdokuSolver) context;
		calculate = sumdoku.calculate;
		solve = sumdoku.solve;
		N=calculate.SUMDOKU_SIZE;
		field = new Cell[N * N];
		int location=0;
		for (int i=0;i<N;i++){
			for (int k=0;k<N;k++){
				Cell cell = new Cell(10*i+k,sumdoku);
				cell.saveValue(0);
				field[location]=cell;
				location++;
			
			}
		}

	}
	Context getcontext(){
		Context context = sumdoku;
		return context;
	}


	// ---=== SOLVE ===---
	public int solving(){
		Data dataCopy = copy(this);
		dataCopy.filldata();

		int result = NOTCOMPLETE; 
		result = dataCopy.solve();

		this.copy_from(dataCopy);		
		return result;
	}
	
	
	
	
	private Data copy(Data data) {
		Data copy = new Data(sumdoku);
		copy.copy_from(data);
		return copy;
	}


	private int solve() {
		return solve.solve(this).checkresult();
		
	}
	
	public void copy_from(Data data){
		this.sums.clear();
		for (Sum sum : data.sums) {
			Sum sum_copy = new Sum(sum.sum, sum.cell_nums, sumdoku);
			this.sums.add(sum_copy);
		}
		for (int i=0;i<data.field.length;i++){
			Cell orig_cell=data.field[i];
			this.field[i]=new Cell(orig_cell.num, orig_cell.var_value, sumdoku);
		}
	}


	// ---=== FILL DATA ===---	
	private void filldata(){
		for (Cell cell : field) {
			setValue(cell.getValue(),cell.num);
		}
		for (Sum sum : sums){
			fill_by_sum(sum.cell_nums, sum.sum);
		}
	}

	public void fill_by_sum(Vector<Integer> cell_nums, int sum) {
		//Log.v(TAG, "calculate.binaryset("+cell_nums.size()+", "+ sum+"):"+calculate.binaryset(cell_nums.size(), sum));
		int var = calculate.binarysettoint(calculate.binaryset(cell_nums.size(), sum));
		for (Integer num : cell_nums) {
			int location=calculate.num_to_location(num);
			if (field[location].getValue() == 0) {
				field[location].var_value &= var;
			}
		}
	}
	
	/**
	 * Set value to selected cell in field, and remove this value from all variant sets of neighbor cells
	 * @param value - int, value to set
	 * @param cell_num - int, number of cells where value should be set
	 */
	public void setValue(int value, int cell_num){
		Cell cell = field[calculate.num_to_location(cell_num)];
		cell.saveValue(value);
		//if (value!=0) {
			//remove_from_sum(cell_num,value);
			//remove_from_field(cell);
			//Log.v(TAG, "cell "+cell_num+" = "+v);
		//}
	}

	
//---=== REMOVE ===---	

	/**
	 * Removes value of cell from variants of all cells from same row, column and square
	 * @param cell - cell with value we want to remove
	 */
	public void remove_from_field(Cell cell) {
		//Log.v(TAG, "remove_from_field cell N "+cell.num+"("+solve.vars_to_string(cell.var_value)+")");
		Vector<Integer> except = new Vector<Integer>();
		except.add(cell.num);
		int rowpart = (cell.num / 10) / (N / 3);
		int columnpart = (cell.num % 10) / (N / 3);
		remove_from_column(cell.getValue() - 1, cell.num % 10, except);
		remove_from_row(cell.getValue() - 1, cell.num / 10, except);
		remove_from_square(cell.getValue() - 1, rowpart, columnpart, except);
	}

	public boolean remove_from_row(int n, int row, Vector<Integer> except) {
		boolean nochanges = true;
		for (int i = 0; i < N; i++) {
			if (!except.contains((N + 1) * row + i)) {
				// System.out.println("fields not contains "+((N+1)*row+i));
				int old = field[N * row + i].var_value;
				//field[N * row + i].var_value &= ~(1 << n);
				//field[N * row + i].var_value = calculate.remove_var(n+1, old);
				field[N * row + i].remove_var(n+1);
				if (old != field[N * row + i].var_value) {
					nochanges = false;
					// System.out.println("CHANGED ROW "+row);
				}
			}
		}
		return nochanges;
	}

	public boolean remove_from_column(int n, int column, Vector<Integer> except) {
		boolean nochanges = true;
		for (int i = 0; i < N; i++) {
			if (!except.contains((N + 1) * i + column)) {
				// System.out.println("fields not contains "+((N+1)*i+column));
				//nochanges = !field[N * i + column].remove_var(n);
				int old = field[N * i + column].var_value;
				field[N * i + column].remove_var(n+1);
				if (old != field[N * i + column].var_value) {
					nochanges = false;
					// System.out.println("CHANGED column "+column);
				}
			}
		}
		return nochanges;
	}

	public boolean remove_from_square(int n, int rowpart, int columnpart, Vector<Integer> except) {
		boolean nochanges = true;
		// System.out.println("remove "+(n+1)+" from square ("+rowpart+","+columnpart+")");
		for (int i = N / 3 * rowpart; i < N / 3 * (rowpart + 1); i++) {
			for (int k = N / 3 * columnpart; k < N / 3 * (columnpart + 1); k++) {
				if (!except.contains(10 * i + k)) {
					//nochanges = !field[N * i + k].remove_var(n);
					// System.out.println("want remove "+(n+1)+" from cell ("+i+","+k+"), field["+(N*i+k)+"]");
					int old = field[N * i + k].var_value;
					field[N * i + k].remove_var(n+1);
					if (old != field[N * i + k].var_value) {
						nochanges = false;
					}
				}
			}
		}
		return nochanges;
	}
	
// Cell
	
	public Cell getcell(int num) {
		//Log.v(TAG, "getcell("+num+")");
		//printfield();
		return field[calculate.num_to_location(num)];
	}

//Sum
	public void addtosums(Sum s) {
		if (s.sum!=0) sums.add(s);
	}
	public Vector<Sum> getsums() {
		return sums;
	}
	public void clearsums() {
		sums.clear();
	}


	public void setfield(Cell cell) {
		field[calculate.num_to_location(cell.num)]=cell;
	}

	public void clearfield() {
		for (Cell cell:field) {
			cell.var_value=0;
			cell.saveValue(0);
		}
		
	}
	public  int allvars(Vector<Cell> set) {
		int result = 0;
		for (Cell cell : set) {
			result |= cell.var_value;
		}
		return result;
	}
	
	public Sum getsum(int cellnum) {
		for (Sum sum : sums) {
			if (sum.cell_nums.contains(cellnum)){
				return sum;
			}
		}
		return null;
	}
	public void remove_from_sum(int cellnum, int value) {
		for (Sum sum : sums) {
			if (sum.cell_nums.contains(cellnum)){
				for (int cell_num : sum.cell_nums) {
					if (cell_num!=cellnum){
						Cell cell = field[calculate.num_to_location(cell_num)];
						cell.remove_var(value);
					}
				}
				return;
			}
		}
	}

	public void printfield() {
		Log.v(TAG, "printfield()");
		for (Sum sum : sums) {
			Log.v(TAG, "Sum: "+sum.sum+", "+sum.cell_nums.size()+" cells: "+sum.cell_nums);
		}
		int n = 0;
		for (int i = 0; i < N; i++) {
			String s = "";
			for (int k = 0; k < N; k++) {
				String var = "";
				Cell c = field[n++];
				var=calculate.vars_to_string(c.var_value);
				s += c.getValue() + "(" + var + ") ";
			}
			Log.v(TAG, s);
		}
	}


	public String getWronginput() {
		return wronginput;
	}


	public void setWronginput(String wronginput) {
		this.wronginput = wronginput;
	}


	public int checkresult() {
		Log.v(TAG, "Checking result...");
		//printfield();
		for (Cell cell : field) {
			if  (cell.var_value==0) {
				return EMPTYCELL;
			}
			if  (cell.getValue()==0) {
				return NOTCOMPLETE;
			}
		}
		return SOLVED;
	}


	public Data solving_by_suggestions() {
		return solve.solving_by_suggestions(this);
	}


	/**
	 * Generate new var_values for each cell without value.
	 * Use when user make changes in field which conflicts with pre-solved var_values 
	 */
	public void new_vars() {
		for (Cell cell:field){
			if (cell.getValue()==0){
				cell.saveValue(0);
			}
		}
	}
	
	public int find_possible_vars(Cell cell) {
		int vars;
//		if (cell.getValue()==0) {vars = cell.var_value;}
//		else {vars=calculate.max_var();}
		vars=calculate.max_var();
		int row = cell.num / 10;
		int column = cell.num % 10;
		//see in lines 10 * row + i, 10 * i + column
		for (int i = 0; i < N; i++) {
			if(i!=column){
				int value = getcell(10 * row + i).getValue();
//				Log.v(TAG, "row="+row+", i="+i+",(10 * row + i)="+(10 * row + i)+", value="+value);
				if (value!=0){vars=calculate.remove_var(value,vars);}
			}
			if(i!=row){
				int value = getcell(10 * i + column).getValue();
//				Log.v(TAG, "column="+column+", i="+i+",(10 * i + column)="+(10 * i + column)+", value="+value);
				if (value!=0){vars=calculate.remove_var(value,vars);}
			}
		}
		
		//see in squares
		int rowpart = row / (N / 3);
		int columnpart = column / (N / 3);
		for (int i = N / 3 * rowpart; i < N / 3 * (rowpart + 1); i++) {
			for (int k = N / 3 * columnpart; k < N / 3 * (columnpart + 1); k++) {
				if ((10 * i + k)!=cell.num) {
					int value = getcell(10 * i + k).getValue();
					if (value!=0){vars=calculate.remove_var(value,vars);}
				}
			}
		}
		
		//see in sums
		Sum sum=getsum(cell.num);
		if (sum!=null){
			for (int cell_num : sum.cell_nums) {
				if (cell_num!=cell.num){
					int value = getcell(cell_num).getValue();
					if (value!=0){vars=calculate.remove_var(value,vars);}
				}
			}
			vars=calculate.intersection(vars, calculate.all_vars(sum));
		}
		
//		Log.v(TAG, "Possible vars: "+calculate.vars_to_string(vars));
		return vars;
	}

}
