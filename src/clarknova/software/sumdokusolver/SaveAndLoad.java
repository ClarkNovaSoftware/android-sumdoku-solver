package clarknova.software.sumdokusolver;

import java.util.Calendar;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SaveAndLoad extends Activity {

	private View newsave;
	private ListView saves;
	private TextView editname;
	private String save_name = "";

	Intent intent = new Intent();
	private DBAdapter db = new DBAdapter(this);
//	private Context context;
	private String TAG = "sumdoku";
	private String NAME = "name";
	private final String NOSAVES = "No saves";
	private static final String FIELDSTATE = "field state";
	
	private String[] savenames;

	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.save_and_load_activity);
//		context = getApplicationContext();
//		TAG = context.getResources().getString(R.string.tag);
//		NAME = context.getResources().getString(R.string.name);

		
		savenames=takesavenames();
		
		newsave = findViewById(R.id.view_newsave);
		editname = (EditText) newsave.findViewById(R.id.text_newsave);
		editname.setOnEditorActionListener(
		        new EditText.OnEditorActionListener() {
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_DONE) {
		        	newsave();
		            return true;
		        }
		        return false;
		    }
		});
		ImageButton addbutton = (ImageButton) newsave
				.findViewById(R.id.button_add_new_save);
		addbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				newsave();
			}
		});

		saves = (ListView) findViewById(R.id.list_saves);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.saved_item, R.id.saved_item, savenames);
		saves.setAdapter(adapter);

		saves.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int position, long id) {
				if (!( (String)((TextView) view).getText() ).equals(NOSAVES)){
					deletesave(view);
				}
				return false;
			}
		});

		Intent sender = getIntent();
		int mode = sender.getIntExtra(AndroidSumdokuSolver.MODE, 0);

		switch (mode) {
		case AndroidSumdokuSolver.SAVE:
			save();
			break;
		case AndroidSumdokuSolver.LOAD:
			load();
			break;

		default:
			Log.e(TAG, "No extras ganed");
			break;
		}

	}

	private void deletesave(View view) {

		final String name = (String) ((TextView) view).getText();

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setMessage(
				String.format(
						getResources().getString(R.string.message_remove_save),
						name)).setTitle(R.string.title_remove_save);

		builder.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						db.clearSaving(name);
						dialog.cancel();
						regenerateList();
					}
				});
		builder.setNegativeButton(android.R.string.no,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	protected void regenerateList() {
		String[] values = takesavenames();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.saved_item, R.id.saved_item, values);
		saves.setAdapter(adapter);
	}

	private String[] takesavenames() {
		String[] names;
		Vector<String> savenamesvector;
		savenamesvector = db.allsavenames();
		savenamesvector.remove(FIELDSTATE);
		
		if (savenamesvector==null || savenamesvector.size()==0){
			savenamesvector=new Vector<String>();
			savenamesvector.add(NOSAVES);
		}
		
		names = new String[savenamesvector.size()];
		int i=0;
		for (String name : savenamesvector) {
			names[i++]=name;
		}
		
		return names;
		
	}

	private void newsave() {
		save_name = click_newsave();
		if (save_name.equals(NOSAVES) || save_name.equals(FIELDSTATE)){
			Log.v(TAG, "Wrong save name: "+save_name);
			editname.setText("");
			
		}
		else if (nameExists(save_name)) {
			ask_rewrite();
		} else {
			Log.v(TAG, "Not exist, saving");
			Intent getback = new Intent();
			getback.putExtra(NAME, save_name);
			setResult(RESULT_OK, getback);
			finish();
		}

	}

	/**
	 * Ask user if they want to rewrite existing save
	 */
	private void ask_rewrite() {
//		Log.v(TAG, "Ask user if they want rewrite");
		final String n = save_name;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				String.format(
						getResources().getString(
								R.string.message_rewrite_save), n))
				.setTitle(R.string.title_rewrite_save);

		builder.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent getback = new Intent();
						getback.putExtra(NAME, save_name);
						setResult(RESULT_OK, getback);
						finish();
						dialog.cancel();
					}
				});
		builder.setNegativeButton(android.R.string.no,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						editname.setText(null);
						dialog.cancel();
					}
				});

		AlertDialog dialog = builder.create();
		dialog.show();
		
	}

	private void load() {

		newsave.setVisibility(View.INVISIBLE);
		((TextView) findViewById(R.id.text_select_saved)).setText("Load");
		saves.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				save_name = (String) ((TextView) view).getText();
				if (!save_name.equals(NOSAVES)){
					Log.v(TAG, "(in SaveLoad.java) Load save_name " + save_name);
					intent.putExtra(NAME, save_name);
					setResult(RESULT_OK, intent);
					finish();
				}
				return;
			}
		});

	}

	private void save() {

		saves.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				save_name = (String) ((TextView) view).getText();
				if (!save_name.equals(NOSAVES)){
//					Log.v(TAG, "(in SaveLoad.java) Save save_name " + save_name);
//					intent.putExtra(NAME, save_name);
//					setResult(RESULT_OK, intent);
//					finish();
					ask_rewrite();
				}
			}
		});
	}

	public String click_newsave() {
		
		String name = editname.getText().toString().trim();
		if (name.equals(null) || name.equals("")) {
			Log.i(TAG, "Empty name, so I'll generate it");
			Calendar c = Calendar.getInstance();
			name = c.get(Calendar.DAY_OF_YEAR) + "-"
					+ c.get(Calendar.HOUR_OF_DAY) + ":"
					+ c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
			// Long t = System.currentTimeMillis()/1000;
			// name = t.toString();
		}
		Log.i(TAG, "name==" + name);
		return name;

	}

	private boolean nameExists(String name) {
		Log.v(TAG, name);
		for (String n : savenames) {
			Log.v(TAG, name + " - " + n);
			if (n.equals(name)) {
				Log.v(TAG, n + " already exists");
				return true;
			}
		}
		return false;
	}

}
